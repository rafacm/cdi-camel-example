package org.fusesource.example.cdi.camel;

import org.apache.camel.component.cdi.CdiCamelContext;

import javax.inject.Singleton;

/**
 * Class annotated with @Singleton in order to avoid having multiple instances of
 * the CdiCamelContext class.
 *
 * Singleton CDI {@link org.apache.camel.CamelContext} class.
 */
@Singleton
public class CamelContextApplicationScoped extends CdiCamelContext {
    public CamelContextApplicationScoped() {
        super();
    }
}
