package org.fusesource.example.cdi.camel;

import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * This is a simple service class to test access to the Apache Camel context
 *
 * User: rafa
 * Date: 8/10/12
 * Time: 7:59 PM
 */
@Named("pingPongService")
public class PingPongService {

    Logger logger = LoggerFactory.getLogger(PingPongService.class);

    String pongEndpointUri = "direct:pong";

    @Inject
    CamelContextApplicationScoped camelCtx;

    public void ping(String msg) {
        logger.info("Received ping message: " + msg);

        ProducerTemplate producerTemplate = camelCtx.createProducerTemplate();
        // send the message back to Camel
        producerTemplate.sendBody(pongEndpointUri, msg);
    }

    public void setPongEndpointUri(String pongEndpointUri) {
        this.pongEndpointUri = pongEndpointUri;
    }
}
