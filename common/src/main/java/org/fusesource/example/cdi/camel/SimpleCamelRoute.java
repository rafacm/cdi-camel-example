package org.fusesource.example.cdi.camel;

import org.apache.camel.builder.RouteBuilder;

/**
 * User: charlesmoulliard
 * Date: 16/02/12.
 */
public class SimpleCamelRoute extends RouteBuilder {

    private String timerUri;
    private String pingEndpointUri;
    private String pongEndpointUri;

    public SimpleCamelRoute() {
        System.out.println(">> SimpleCamelRoute instantiated");
    }

    @Override
    public void configure() throws Exception {

        from(timerUri)
            // Set Body with text "Bean Injected"
            .setBody().simple("Bean Injected")
            // Lookup for bean injected by CDIcontainer
            .beanRef("helloWorld", "sayHello")
            // Display response received in log when calling HelloWorld
            .log(">> Response : ${body}")
            .to(pingEndpointUri);

        from(pongEndpointUri)
            .log(">> Pong : ${body}");
    }

    public void setTimerUri(String timerUri) {
        this.timerUri = timerUri;
    }

    public void setPingEndpointUri(String pingEndpointUri) {
        this.pingEndpointUri = pingEndpointUri;
    }

    public void setPongEndpointUri(String pongEndpointUri) {
        this.pongEndpointUri = pongEndpointUri;
    }
}